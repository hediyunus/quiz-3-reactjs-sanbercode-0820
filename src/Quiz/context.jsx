import React, { useState, createContext, useEffect } from "react";
import axios from 'axios';

export const FilmContext = createContext();

export const FilmProvider = props => {
  const [log, setLogin] = useState({username:"", password:""})

  const [dataFilm, setdataFilm] = useState(null)
  const [input, setInput] = useState({title: "", description: "", year: 2020, duration:120, genre:"",
  rating:0, image_url:"", id: null})

  useEffect(() => {
    if (dataFilm === null){
      axios.get(`http://backendexample.sanbercloud.com/api/movies`)
      .then(res => {
        setdataFilm(res.data)
      })
    }
  }, [dataFilm]);
  return (
    <FilmContext.Provider value={[dataFilm, setdataFilm, input, setInput, log, setLogin]}>
      {props.children}
    </FilmContext.Provider>
  );
};
