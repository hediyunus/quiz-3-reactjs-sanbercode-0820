
import React from "react"
import { Link } from "react-router-dom";
import './public/css//style.css';
import logo from './public/img//logo.png';

const Nav = () => {
  return (
    <>

      <div>
          <header>
          <img src={logo} alt="logo"  width="200px" />
    
        <nav>

          <ul>
            <Link to="/" style={{ marginRight: "12px" }}>Home</Link>
            <Link to="/about" style={{ marginRight: "12px" }}> About</Link>
            <Link to="/movies" style={{ marginRight: "12px" }}> Movie List Editor</Link>
            <Link to="/login" style={{ marginRight: "12px" }}>Logout</Link>

          </ul>
        </nav>
        </header>
      
      </div>
    </>
  );
};

export default Nav;
