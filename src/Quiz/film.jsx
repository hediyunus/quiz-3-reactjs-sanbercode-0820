import React from "react"
import { FilmProvider } from "./context.jsx"
import Routes from "./router.jsx"

const Film = () => {
  return (
    <FilmProvider>
      <Routes />
      <footer>
        <h5>copyright &copy; 2020 by Sanbercode</h5>
      </footer>
    </FilmProvider>
  )
}

export default Film
