
import React, { useContext, Component } from "react";
import { FilmContext } from "./context";
import './public/css//style.css';

class FilmInfo extends Component {
    
    render() {
        const img = {
            width: "60%",
            "object-fit": "cover",
            height: "300px",
            "border-top-left-radius": "10px",
            "border-top-right-radius": "10px",
            float: "left"
        }
        const card = {
            display: "inline-block",
            width: "100%",
            margin: "10px",
            "border-radius": "10px",
            "box-shadow": "#aaa"
        }

        const container = {
            padding: "10px",
            display: "inline-block",
            width: "100%",
        }
        const fullpage = {
            padding: "10px",
            width: "100%",
        }
        const sidepage = {
            padding: "10px",
            float: "right",
            width: "36%",
        }

        return (
            <div style={container}>
                <div style={fullpage}>
                    <h3><b> {this.props.item.title}</b></h3>
                    <img src={this.props.item.image_url} style={img} alt="Gambar" />
                    <div style={sidepage}>
                        <p><strong style={{ width: "100px"}}>Rating : {this.props.item.rating}</strong></p>
                        <p><strong style={{ width: "100px"}}>Durasi : {this.props.item.duration} </strong></p>
                        <p><strong style={{ width: "100px"}}>Genre : {this.props.item.genre} </strong></p>
            
                    </div>
                </div>
                <div style={card}>
                    <p><strong style={{ width: "100px"}}>Durasi : </strong>{this.props.item.description}</p>
                </div>
                <hr/>
            </div>
        )
    }
}

const Home = () => {
    const [dataFilm] = useContext(FilmContext)

    return (
        <>
            <section>
                <h1>Daftar Film-Film Terbaik</h1>
                {
                    dataFilm !== null && dataFilm.map((item, index) => {
                        return (
                            <FilmInfo key={index} item={item} />
                        )
                    })
                }
            </section>
           
        </>
    );
};

export default Home;

