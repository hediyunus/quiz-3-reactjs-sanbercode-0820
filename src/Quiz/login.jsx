import React, { useContext } from "react"
import { FilmContext } from "./context"
import { useHistory } from "react-router-dom";
const Login = () => {
    const history = useHistory();
    const [, , , , log, setLogin] = useContext(FilmContext)

    const handleChange = (event) => {
        let typeOfInput = event.target.name
        switch (typeOfInput) {
            case "username":
                {
                    setLogin({ ...log, username: event.target.value });
                    break
                }
            case "password":
                {
                    setLogin({ ...log, password: event.target.value });
                    break
                }
            default:
                { break; }
        }
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        if (log.username === "admin" && log.password === "admin") {
            
            history.push("/");
        }
        else {
            alert("Username atau Password anda Salah")

        }


    }

    return (
        
        <section>
            <h1 style={{ textAlign: "center" }}>Login Sistem</h1>
            <div style={{ width: "50%", margin: "0 auto", display: "block" }}>
                <div style={{ border: "1px solid #aaa", padding: "20px" }}>
                    <form onSubmit={handleSubmit}>
                        <label style={{ float: "left" }}>
                            Username
                            </label>
                        <input type="text" style={{ float: "right" }} placeholder="Masukan username anda" name="username" value={log.username} onChange={handleChange} required />
                        <br />
                        <br />
                        <label style={{ float: "left" }}>
                            Password :
                                </label>
                        <input type="password" style={{ float: "right" }} placeholder="Password" name="password" value={log.password} onChange={handleChange} />
                        <br />
                        <br />
                        <div style={{ width: "100%", paddingBottom: "20px" }}>
                            <button style={{ float: "center" }}>Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    );

}

export default Login;