import React, { useContext, useState, useEffect } from "react"
import { FilmContext } from "./context"
import axios from 'axios';
import { Link } from "react-router-dom";

const FilmForm = () => {
    const [dataFilm, setdataFilm, input, setInput] = useContext(FilmContext)
    const [search, setSearch] = useState('')
    const [dataFilter, setFilter] = useState([]);

    const submitForm = (event) => {
        event.preventDefault()

        if (input.id === null) {
            axios.post(`http://backendexample.sanbercloud.com/api/movies`, {
                title: input.title, description: input.description, year: input.year,
                duration: input.duration, genre: input.genre, rating: input.rating, image_url: input.image_url
            })
                .then(res => {
                    var data = res.data
                    setdataFilm([...dataFilm, {
                        id: data.id, title: data.title, description: data.description, year: data.year,
                        duration: data.duration, genre: data.genre, rating: data.rating, image_url: data.image_url
                    }])
                    setInput({
                        id: null, title: "", description: "", year: 2020, duration: 120, genre: "",
                        rating: 0, image_url: ""
                    })
                })

        } else {

            axios.put(`http://backendexample.sanbercloud.com/api/movies/${input.id}`, {
                title: input.title, description: input.description, year: input.year,
                duration: input.duration, genre: input.genre, rating: input.rating, image_url: input.image_url
            })
                .then(res => {
                    var newdataFilm = dataFilm.map(x => {
                        if (x.id === input.id) {
                            x.title = input.title
                            x.description = input.description
                            x.year = input.year
                            x.duration = input.duration
                            x.genre = input.genre
                            x.rating = input.rating
                            x.image_url = input.image_url
                        }
                        return x
                    })
                    setdataFilm(newdataFilm)
                    setInput({
                        id: null, title: "", description: "", year: 2020, duration: 120, genre: "",
                        rating: 0, image_url: ""
                    })
                })
        }
    }
    useEffect(() => {
        setFilter(
            dataFilm.filter((film) =>
           film.title === null ? film.title : film.title.toLowerCase().includes(search.toLowerCase())
          )
        );
      }, [search, dataFilm]);

    const handleChange = (event) => {
        let value = event.target.name
        switch (value) {
            case "title":
                {
                    setInput({ ...input, title: event.target.value })
                    break
                }
            case "description":
                {
                    setInput({ ...input, description: event.target.value })
                    break
                }
            case "year":
                {
                    setInput({ ...input, year: event.target.value })
                    break
                }
            case "duration":
                {
                    setInput({ ...input, duration: event.target.value })
                    break
                }
            case "genre":
                {
                    setInput({ ...input, genre: event.target.value })
                    break
                }
            case "rating":
                {
                    setInput({ ...input, rating: event.target.value })
                    break
                }
            case "image_url":
                {
                    setInput({ ...input, image_url: event.target.value })
                    break
                }
            default:
                { break; }
        }
    }

    const editForm = (event) => {
        console.log("cekk", event.target.value);
        var idFilm = parseInt(event.target.value)
        var Film = dataFilm.find(x => x.id === idFilm)
        setInput({ ...input, id: idFilm, title: Film.title, description: Film.description, year: Film.year,
            duration: Film.duration, genre: Film.genre, rating: Film.rating, image_url: Film.image_url  })

    }

    const deleteFilm = (event) => {
        var idFilm = parseInt(event.target.value)
        axios.delete(`http://backendexample.sanbercloud.com/api/movies/${idFilm}`)
            .then(res => {
                var newdataFilm = dataFilm.filter(x => x.id !== idFilm)
                setdataFilm(newdataFilm)
            })
    }

    return (
        <>
            <section>
                <div>
                    <h1 style={{ textAlign: "center" }}>Daftar Film Terbaik</h1>
                    <input type="text" placeholder="Search" onChange={e => setSearch(e.target.value) }/> <br/> <br/> 
                    <table id='daftar'>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Year</th>
                                <th>Duration</th>
                                <th>Genre</th>
                                <th>Rating</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                dataFilm !== null && dataFilter.map((item, index) => {
                                    return (
                                        <tr key={item.id}>
                                            <td>{index + 1}</td>
                                            <td>{item.title}</td>
                                            <td>{item.description === null ? item.description : item.description.substring(0, 20)} ...</td>
                                            <td>{item.year}</td>
                                            <td>{item.duration}</td>
                                            <td>{item.genre}</td>
                                            <td>{item.rating}</td>
                                            <td>
                                                <button value={item.id} style={{ marginRight: "5px" }} onClick={editForm}>Edit</button>
                                                <button value={item.id} onClick={deleteFilm}>Delete</button>
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                    </table>
                    <br />
                    <h1 style={{ textAlign: "center" }}>Movies Form</h1>
                    <div style={{ width: "50%", margin: "0 auto", display: "block" }}>
                        <div style={{ border: "1px solid #aaa", padding: "20px" }}>
                            <form onSubmit={submitForm}>
                                <label style={{ float: "left" }}>
                                    Title :
                                </label>
                                <input style={{ float: "right" }} type="text" name="title" value={input.title} onChange={handleChange} required />
                                <br />
                                <br />
                                <label style={{ float: "left" }}>
                                    Description :
                                </label>
                                <textarea style={{ float: "right" }}  name="description" value={input.description} onChange={handleChange} required />
                                <br />
                                <br />
                                <label style={{ float: "left" }}>
                                    Year :
                                </label>
                                <input style={{ float: "right" }} type="number" min="1980" name="year" value={input.year} onChange={handleChange} required />
                                <br />
                                <br />
                                <label style={{ float: "left" }}>
                                    Duration :
                                 </label>
                                <input style={{ float: "right" }} type="number" name="duration" value={input.duration} onChange={handleChange} required />
                                <br />
                                <br />
                                <label style={{ float: "left" }}>
                                    Genre :
                                 </label>
                                <input style={{ float: "right" }} type="text" name="genre" value={input.genre} onChange={handleChange} required />
                                <br />
                                <br />
                                <label style={{ float: "left" }}>
                                    Rating :
                                </label>
                                <input style={{ float: "right" }} type="number" min="0" max="10" name="rating" value={input.rating} onChange={handleChange} required />
                                <br />
                                <br />
                                <label style={{ float: "left" }}>
                                    Image Url :
                                </label>
                                <textarea style={{ float: "right", width: "300px"  }}  name="image_url" value={input.image_url} onChange={handleChange} required />
                                <br />
                                <br />
                                <div style={{ width: "100%", paddingBottom: "20px" }}>
                                    <button style={{ marginLeft: "200px" }}>Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <Link to="/">Kembali ke Home</Link>

            </section>
        </>
    )

}

export default FilmForm
