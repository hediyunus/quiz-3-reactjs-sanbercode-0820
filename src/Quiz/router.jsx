
import React, {useContext} from "react"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import About from './about.jsx';
import Home from './home.jsx';
import FilmForm from './crudmovie.jsx';
import Login from './login.jsx';
import Nav from './navigation.jsx';
import NavPub from './navpublic.jsx';
import './public/css//style.css';
import { FilmContext } from "./context"

const Routes = () => {
  const [ , , , , log] = useContext(FilmContext)
if (log.username === "admin" && log.password === "admin") {
  return (
    <>
      <Router>
        <Nav />
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/movies">
            <FilmForm />
          </Route>
          <Route path="/login">
            <Login />
          </Route>
        </Switch>
      </Router>
    </>
  );
}
else {
  return (
    <>
      <Router>
        <NavPub />
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/movies">
            <Login />
          </Route>
          <Route path="/login">
            <Login />
          </Route>
        </Switch>
      </Router>
    </>
  );
}

};

export default Routes;
